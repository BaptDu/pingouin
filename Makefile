start:
	php bin/console server:start 0.0.0.0:8000

stop:
	php bin/console server:stop

install:
	composer install
	make fixtures

fixtures:
	php bin/console doctrine:database:drop --force --if-exists
	php bin/console doctrine:database:create
	php bin/console doctrine:schema:update --force
	php bin/console app:fixtures
	php bin/console fos:user:create fosadmin a@a.fr admin
	php bin/console fos:user:promote fosadmin ROLE_ADMIN
	php bin/console fos:user:create fosuser b@b.fr user
