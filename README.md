# Pingouin 


The penguin project aims to make known the penguins that is on the world.
The users will be able to share their knowledge on the species we have made available but also to note the penguin.

## Built With

* [Symfony](https://symfony.com/doc/current/index.html) - The web framework used
* [Bootstrap](https://getbootstrap.com/docs/4.0/getting-started/introduction/) - Assets for website
* [FOSUserBundle](https://symfony.com/doc/current/bundles/FOSUserBundle/index.html) - Management User


## Links

* [Homepage](http://localhost:1337/)  
* [List penguin](http://localhost:1337/list)  
* [Detail pingouin](http://localhost:1337/detail/{id})  
* [Penguin Management](http://localhost:1337/pingouin)
* [Review Management](http://localhost:1337/review)  

## Install the project

To start the project on your machine.

To tape it:
```
git clone https://github.com/BaptDu/pingouin
vagrant up
vagrant ssh
```

Once everything was installed you could do the make commands

## Commands MakeFile
First command :
```
make install
```
Second command :
```
make start
make stop
```

beware if you have a problem with the MakeFile you have to reveal the php

Example :
````$xslt
php bin/console doctrine:database:drop --force --if-exists
````
becomes 
````$xslt
bin/console doctrine:database:drop --force --if-exists
````
## Authors

* **Baptiste Dumont and Jeremie Vancutsem** 



