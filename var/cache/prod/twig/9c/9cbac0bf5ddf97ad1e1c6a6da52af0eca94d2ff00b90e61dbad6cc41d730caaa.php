<?php

/* PingouinBundle:UI:footer.html.twig */
class __TwigTemplate_c0e543a32d54a8408a89ee226af2641d73c39f47d1e54aa8de7e7b2bfe96687d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"footerwrap\">
\t<div class=\"container\">
\t\t<div class=\"row\">
\t\t\t<div class=\"col-lg-4\">
\t\t\t\t<h4>Social Links</h4>
\t\t\t\t<div class=\"hline-w\"></div>
\t\t\t\t<p>
\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-dribbble\"></i></a>
\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-facebook\"></i></a>
\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-twitter\"></i></a>
\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-instagram\"></i></a>
\t\t\t\t\t<a href=\"#\"><i class=\"fa fa-tumblr\"></i></a>
\t\t\t\t</p>
\t\t\t</div>
\t\t</div>";
    }

    public function getTemplateName()
    {
        return "PingouinBundle:UI:footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "PingouinBundle:UI:footer.html.twig", "/vagrant/pingouin/src/PingouinBundle/Resources/views/UI/footer.html.twig");
    }
}
