<?php

/* PingouinBundle:bootstrap:layout.html.twig */
class __TwigTemplate_a7c1e254523f9713d254f3f34d78adb607a180ff76adf6fe90af5b631b5bcdb1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'stylesheets' => array($this, 'block_stylesheets'),
            'title' => array($this, 'block_title'),
            'nametitle' => array($this, 'block_nametitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
<head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    ";
        // line 5
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 6
        echo "    <title>";
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
     <!-- Bootstrap Core CSS -->
    <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <!-- MetisMenu CSS -->
    <link href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/style.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
    <!-- Morris Charts CSS -->
    <link href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/font-awesome.min.css"), "html", null, true);
        echo "\" rel=\"stylesheet\">
</head>
";
        // line 14
        $this->loadTemplate("PingouinBundle:UI:menu.html.twig", "PingouinBundle:bootstrap:layout.html.twig", 14)->display($context);
        // line 15
        echo "<body>
";
        // line 16
        $this->displayBlock('content', $context, $blocks);
        // line 19
        $this->loadTemplate("PingouinBundle:UI:footer.html.twig", "PingouinBundle:bootstrap:layout.html.twig", 19)->display($context);
        // line 20
        echo "
</body>";
    }

    // line 5
    public function block_stylesheets($context, array $blocks = array())
    {
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "Pingouin - ";
        $this->displayBlock('nametitle', $context, $blocks);
    }

    public function block_nametitle($context, array $blocks = array())
    {
    }

    // line 16
    public function block_content($context, array $blocks = array())
    {
        // line 17
        echo "
";
    }

    public function getTemplateName()
    {
        return "PingouinBundle:bootstrap:layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 17,  82 => 16,  71 => 6,  66 => 5,  61 => 20,  59 => 19,  57 => 16,  54 => 15,  52 => 14,  47 => 12,  42 => 10,  37 => 8,  31 => 6,  29 => 5,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "PingouinBundle:bootstrap:layout.html.twig", "/vagrant/pingouin/src/PingouinBundle/Resources/views/bootstrap/layout.html.twig");
    }
}
