<?php

/* TwigBundle:Exception:error.html.twig */
class __TwigTemplate_26e0d9c16a782666e706d14244bde63ba0922ae1c06f646f82e5a1fb17b94f66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
    <title>An Error Occurred: ";
        // line 5
        echo twig_escape_filter($this->env, ($context["status_text"] ?? null), "html", null, true);
        echo "</title>
  </head>
  <body>
    <h1>Oops! An Error Occurred</h1>
    <h2>The server returned a \"";
        // line 9
        echo twig_escape_filter($this->env, ($context["status_code"] ?? null), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? null), "html", null, true);
        echo "\".</h2>

    <div>
      Something is broken. Please e-mail us at [email] and let us know
      what you were doing when this error occurred. We will fix it as soon
      as possible. Sorry for any inconvenience caused.
    </div>
  </body>
</html>";
    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  32 => 9,  25 => 5,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "TwigBundle:Exception:error.html.twig", "/vagrant/pingouin/app/Resources/TwigBundle/views/Exception/error.html.twig");
    }
}
