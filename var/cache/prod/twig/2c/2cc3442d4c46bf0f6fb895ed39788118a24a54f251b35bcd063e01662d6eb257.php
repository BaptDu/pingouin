<?php

/* PingouinBundle:Group:group.html.twig */
class __TwigTemplate_2c9e673a21d114cfb066651abd2993d76a93f618ca9dd73c949bf122658d3d7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PingouinBundle:bootstrap:layout.html.twig", "PingouinBundle:Group:group.html.twig", 1);
        $this->blocks = array(
            'nametitle' => array($this, 'block_nametitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PingouinBundle:bootstrap:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_nametitle($context, array $blocks = array())
    {
        echo "Listes";
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "                <div class=\"panel panel-default\">
                    <div class=\"container mtb\">
                        <div class=\"row\">
                            <h3 class=\"panel-title\">Liste des pingouins</h3><br>
                            <table class=\"table table-striped table-bordered table-list\">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Nom de l'espece</th>
                                        <th>Nom du pingouin</th>
                                        <th>Description</th>
                                        <th>    </th>
                                    </tr>
                                </thead>

                                <tr>
                                        ";
        // line 20
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["groups"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["group"]) {
            // line 21
            echo "                                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), $context["group"], "pingouins", array()));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["pingouin"]) {
                // line 22
                echo "                                                <td>
                                                    ";
                // line 23
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["group"], "id", array()), "html", null, true);
                echo "
                                                </td>
                                                <td>
                                                    ";
                // line 26
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["group"], "species", array()), "html", null, true);
                echo "
                                                </td>
                                                <td>
                                                    ";
                // line 29
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["pingouin"], "name", array()), "html", null, true);
                echo "
                                                </td>
                                                <td>
                                                 ";
                // line 32
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["pingouin"], "summary", array()), "html", null, true);
                echo "
                                             </td>
                                             <td>
                                              <a href=\"";
                // line 35
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("pingouin_detail", array("pingouinId" => twig_get_attribute($this->env, $this->getSourceContext(), $context["pingouin"], "id", array()))), "html", null, true);
                echo "\"> En savoir plus</a>
                                          </td>
                                      </tr>
                                  ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 39
                echo "                                    <tr>
                                        <td>Aucun pingouin dans notre base !</td>
                                    </tr>

                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['pingouin'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 44
            echo "                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['group'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "
";
    }

    public function getTemplateName()
    {
        return "PingouinBundle:Group:group.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 52,  112 => 44,  102 => 39,  93 => 35,  87 => 32,  81 => 29,  75 => 26,  69 => 23,  66 => 22,  60 => 21,  56 => 20,  38 => 4,  35 => 3,  29 => 2,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "PingouinBundle:Group:group.html.twig", "/vagrant/pingouin/src/PingouinBundle/Resources/views/Group/group.html.twig");
    }
}
