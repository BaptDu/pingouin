<?php

/* PingouinBundle:UI:menu.html.twig */
class __TwigTemplate_b60f3fc24fc99cf149da54168037bbd4698bc74d61d776dfece3d21b524896e5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo " <!-- Fixed navbar -->
    <div class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">
      <div class=\"container\">
        <div class=\"navbar-header\">
          <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
            <span class=\"sr-only\">Toggle navigation</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
          </button>
          <a class=\"navbar-brand\" href=\"index.html\">SOLID.</a>
        </div>
        <div class=\"navbar-collapse collapse navbar-right\">
          <ul class=\"nav navbar-nav\">
            <li class=\"active\"><a href=\"index.html\">HOME</a></li>
            <li><a href=\"about.html\">ABOUT</a></li>
            <li><a href=\"contact.html\">CONTACT</a></li>
            <li class=\"dropdown\">
              <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">PAGES <b class=\"caret\"></b></a>
              <ul class=\"dropdown-menu\">
                <li><a href=\"blog.html\">BLOG</a></li>
                <li><a href=\"single-post.html\">SINGLE POST</a></li>
                <li><a href=\"portfolio.html\">PORTFOLIO</a></li>
                <li><a href=\"single-project.html\">SINGLE PROJECT</a></li>
              </ul>
            </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "PingouinBundle:UI:menu.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "PingouinBundle:UI:menu.html.twig", "/vagrant/pingouin/src/PingouinBundle/Resources/views/UI/menu.html.twig");
    }
}
