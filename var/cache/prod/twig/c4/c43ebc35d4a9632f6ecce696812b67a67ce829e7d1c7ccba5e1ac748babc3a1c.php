<?php

/* PingouinBundle:Detail:detail.html.twig */
class __TwigTemplate_3cb1ac26f0abb43e7c3011e170a72bdf1efc4dd28d6fca2db3faace1bd10932c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("PingouinBundle:bootstrap:layout.html.twig", "PingouinBundle:Detail:detail.html.twig", 1);
        $this->blocks = array(
            'nametitle' => array($this, 'block_nametitle'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "PingouinBundle:bootstrap:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_nametitle($context, array $blocks = array())
    {
        echo "Detail";
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "        \t\t<div class=\"panel panel-default\">
        \t\t\t<div class=\"container mtb\">
        \t\t\t\t<div class=\"row\">
                            <a href=\"";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("pingouin_list");
        echo "\">Retour a la liste</a>
                            <div class=\"col-lg-4\">
                                <h3>Detail de l'espece : ";
        // line 9
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["pingouin"] ?? null), "species", array()), "html", null, true);
        echo " </h3>
                                <div class=\"hline-w\"></div>
                                <h4>Portant de nom de :  ";
        // line 11
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["pingouin"] ?? null), "name", array()), "html", null, true);
        echo "
                                <p>
                                    <h4>Description :</h4>
                                    ";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), ($context["pingouin"] ?? null), "summary", array()), "html", null, true);
        echo "
                                    </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    ";
    }

    public function getTemplateName()
    {
        return "PingouinBundle:Detail:detail.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 14,  53 => 11,  48 => 9,  43 => 7,  38 => 4,  35 => 3,  29 => 2,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "PingouinBundle:Detail:detail.html.twig", "/vagrant/pingouin/src/PingouinBundle/Resources/views/Detail/detail.html.twig");
    }
}
