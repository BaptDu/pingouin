<?php

/* FrontBundle:Default:index.html.twig */
class __TwigTemplate_a85383320bb08d80f4fc62208e09cc16e67e6ac9f16c052e59e194e227b77f2d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("FrontBundle::base.html.twig", "FrontBundle:Default:index.html.twig", 1);
        $this->blocks = array(
            'childtitle' => array($this, 'block_childtitle'),
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "FrontBundle::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_22556bc744148eb68a17654bdf36706c91460a592f08fe52fb77ea5355e07406 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_22556bc744148eb68a17654bdf36706c91460a592f08fe52fb77ea5355e07406->enter($__internal_22556bc744148eb68a17654bdf36706c91460a592f08fe52fb77ea5355e07406_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontBundle:Default:index.html.twig"));

        $__internal_f8a90f5285091821cf8499e657d4f60586c7ed54ed9126fefa207c1d7a89ac5c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f8a90f5285091821cf8499e657d4f60586c7ed54ed9126fefa207c1d7a89ac5c->enter($__internal_f8a90f5285091821cf8499e657d4f60586c7ed54ed9126fefa207c1d7a89ac5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontBundle:Default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_22556bc744148eb68a17654bdf36706c91460a592f08fe52fb77ea5355e07406->leave($__internal_22556bc744148eb68a17654bdf36706c91460a592f08fe52fb77ea5355e07406_prof);

        
        $__internal_f8a90f5285091821cf8499e657d4f60586c7ed54ed9126fefa207c1d7a89ac5c->leave($__internal_f8a90f5285091821cf8499e657d4f60586c7ed54ed9126fefa207c1d7a89ac5c_prof);

    }

    // line 3
    public function block_childtitle($context, array $blocks = array())
    {
        $__internal_f9076e31906b440ba4b716fce220d3b87bba0fb3e6d456d79a6a78dbf50fd34f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f9076e31906b440ba4b716fce220d3b87bba0fb3e6d456d79a6a78dbf50fd34f->enter($__internal_f9076e31906b440ba4b716fce220d3b87bba0fb3e6d456d79a6a78dbf50fd34f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "childtitle"));

        $__internal_a9d50b5ba80ac0c14eba6e4145afb0cccc4dda98270778b2938309ad2f3f6d5e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a9d50b5ba80ac0c14eba6e4145afb0cccc4dda98270778b2938309ad2f3f6d5e->enter($__internal_a9d50b5ba80ac0c14eba6e4145afb0cccc4dda98270778b2938309ad2f3f6d5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "childtitle"));

        echo "L'accueil";
        
        $__internal_a9d50b5ba80ac0c14eba6e4145afb0cccc4dda98270778b2938309ad2f3f6d5e->leave($__internal_a9d50b5ba80ac0c14eba6e4145afb0cccc4dda98270778b2938309ad2f3f6d5e_prof);

        
        $__internal_f9076e31906b440ba4b716fce220d3b87bba0fb3e6d456d79a6a78dbf50fd34f->leave($__internal_f9076e31906b440ba4b716fce220d3b87bba0fb3e6d456d79a6a78dbf50fd34f_prof);

    }

    // line 4
    public function block_head($context, array $blocks = array())
    {
        $__internal_23c91de960f33a45add7bcd5f95e1014e2c1140357f48ffef0a1d6424b06679e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_23c91de960f33a45add7bcd5f95e1014e2c1140357f48ffef0a1d6424b06679e->enter($__internal_23c91de960f33a45add7bcd5f95e1014e2c1140357f48ffef0a1d6424b06679e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_3f30599c8e688d3de6d0fbb238cadde3a515920cbb6935dc6dc81ae4bf3e6c9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f30599c8e688d3de6d0fbb238cadde3a515920cbb6935dc6dc81ae4bf3e6c9a->enter($__internal_3f30599c8e688d3de6d0fbb238cadde3a515920cbb6935dc6dc81ae4bf3e6c9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_3f30599c8e688d3de6d0fbb238cadde3a515920cbb6935dc6dc81ae4bf3e6c9a->leave($__internal_3f30599c8e688d3de6d0fbb238cadde3a515920cbb6935dc6dc81ae4bf3e6c9a_prof);

        
        $__internal_23c91de960f33a45add7bcd5f95e1014e2c1140357f48ffef0a1d6424b06679e->leave($__internal_23c91de960f33a45add7bcd5f95e1014e2c1140357f48ffef0a1d6424b06679e_prof);

    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        $__internal_a0c3bae65f6fb67cbdb57308f7bb2decace82ffead624fb35c9fca887dab272d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a0c3bae65f6fb67cbdb57308f7bb2decace82ffead624fb35c9fca887dab272d->enter($__internal_a0c3bae65f6fb67cbdb57308f7bb2decace82ffead624fb35c9fca887dab272d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_5f7c834a29305702f574e112f87d87b08fac1af3bdebdad9ca51cf0262ef7fa6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5f7c834a29305702f574e112f87d87b08fac1af3bdebdad9ca51cf0262ef7fa6->enter($__internal_5f7c834a29305702f574e112f87d87b08fac1af3bdebdad9ca51cf0262ef7fa6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 6
        echo "    <div class=\"banner-bottom\">
        <div class=\"container\">
            <h3>Rejoind le club</h3>
            <div class=\"welcome\">
                <img src=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/images/1.jpg"), "html", null, true);
        echo "\" alt=\" \" class=\"img-responsive\" />
                <h4> Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil
                    impedit quo minus id quod maxime placeat facere possimus</h4>
                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                    consectetur, adipisci velit, sed quia non numquam eius modi tempora
                    incidunt ut labore et dolore magnam aliquam  quod maxime
                    placeat facere possimusquaerat voluptatem.</p>
                <div class=\"more\">
                    <a href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
        echo "\" class=\"hvr-bounce-to-bottom\">S'inscrire</a>
                </div>
            </div>
        </div>
    </div>

    <div class=\"events\">
        <div class=\"container\">
            <h3>Events</h3>
            <p class=\"para\">Ut aut reiciendis voluptatibus maiores alias
                consequatur aut perferendis doloribus asperiores repellat incidunt ut labore et dolore magnam aliquam  quod maxime
                placeat facere possimusquaerat voluptatem</p>
            <div class=\"events-grids\">
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left1\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left2\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"clearfix\"> </div>
            </div>
            <div class=\"events-grids\">
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left3\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left4\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left5\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"clearfix\"> </div>
            </div>
            <div class=\"events-grids\">
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left6\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left7\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left8\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"clearfix\"> </div>
            </div>
        </div>
    </div>

";
        
        $__internal_5f7c834a29305702f574e112f87d87b08fac1af3bdebdad9ca51cf0262ef7fa6->leave($__internal_5f7c834a29305702f574e112f87d87b08fac1af3bdebdad9ca51cf0262ef7fa6_prof);

        
        $__internal_a0c3bae65f6fb67cbdb57308f7bb2decace82ffead624fb35c9fca887dab272d->leave($__internal_a0c3bae65f6fb67cbdb57308f7bb2decace82ffead624fb35c9fca887dab272d_prof);

    }

    public function getTemplateName()
    {
        return "FrontBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 18,  92 => 10,  86 => 6,  77 => 5,  60 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"FrontBundle::base.html.twig\" %}

{% block childtitle %}L'accueil{% endblock %}
{% block head %}{% endblock %}
{% block content %}
    <div class=\"banner-bottom\">
        <div class=\"container\">
            <h3>Rejoind le club</h3>
            <div class=\"welcome\">
                <img src=\"{{ asset('assets/images/1.jpg') }}\" alt=\" \" class=\"img-responsive\" />
                <h4> Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil
                    impedit quo minus id quod maxime placeat facere possimus</h4>
                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                    consectetur, adipisci velit, sed quia non numquam eius modi tempora
                    incidunt ut labore et dolore magnam aliquam  quod maxime
                    placeat facere possimusquaerat voluptatem.</p>
                <div class=\"more\">
                    <a href=\"{{ path('fos_user_registration_register') }}\" class=\"hvr-bounce-to-bottom\">S'inscrire</a>
                </div>
            </div>
        </div>
    </div>

    <div class=\"events\">
        <div class=\"container\">
            <h3>Events</h3>
            <p class=\"para\">Ut aut reiciendis voluptatibus maiores alias
                consequatur aut perferendis doloribus asperiores repellat incidunt ut labore et dolore magnam aliquam  quod maxime
                placeat facere possimusquaerat voluptatem</p>
            <div class=\"events-grids\">
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left1\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left2\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"clearfix\"> </div>
            </div>
            <div class=\"events-grids\">
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left3\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left4\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left5\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"clearfix\"> </div>
            </div>
            <div class=\"events-grids\">
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left6\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left7\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"col-md-4 events-grid\">
                    <div class=\"col-xs-3 events-grid-left8\">
                        <span> </span>
                    </div>
                    <div class=\"col-xs-9 events-grid-right\">
                        <h4>Itaque earum rerum hic tenetur doloribus</h4>
                        <p>Ut aut reiciendis voluptatibus maiores alias
                            consequatur aut perferendis doloribus asperiores repellat.</p>
                    </div>
                    <div class=\"clearfix\"> </div>
                </div>
                <div class=\"clearfix\"> </div>
            </div>
        </div>
    </div>

{% endblock %}

", "FrontBundle:Default:index.html.twig", "/vagrant/pingouin/src/FrontBundle/Resources/views/Default/index.html.twig");
    }
}
