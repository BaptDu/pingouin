<?php

/* FrontBundle:UI:menu.html.twig */
class __TwigTemplate_0b2c719a683c2528a0d8f37f6feba19efcfcf2fba25ecde57482bdefeff8669c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5ef6f5767758e1b9acc59d8543f78066c44fd2d5432a1cb9e6a3d49d3dc7df1b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ef6f5767758e1b9acc59d8543f78066c44fd2d5432a1cb9e6a3d49d3dc7df1b->enter($__internal_5ef6f5767758e1b9acc59d8543f78066c44fd2d5432a1cb9e6a3d49d3dc7df1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontBundle:UI:menu.html.twig"));

        $__internal_c1f47c7999d49f7f47cd479411d29e0278b4faa59cb6af4ad0672a5aa627818d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1f47c7999d49f7f47cd479411d29e0278b4faa59cb6af4ad0672a5aa627818d->enter($__internal_c1f47c7999d49f7f47cd479411d29e0278b4faa59cb6af4ad0672a5aa627818d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontBundle:UI:menu.html.twig"));

        // line 1
        echo "<div class=\"header\">
    <div class=\"container\">
        <div class=\"header-nav\">
            <nav class=\"navbar navbar-default\">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                            data-target=\"#bs-example-navbar-collapse-1\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <div class=\"logo\">
                        <a class=\"navbar-brand\" href=\"";
        // line 15
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">Pingouin <span>Base</span></a>
                        <div class=\"leaf\">
                            <span class=\"glyphicon glyphicon-leaf\" aria-hidden=\"true\"></span>
                        </div>
                    </div>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class=\"collapse navbar-collapse nav-wil\" id=\"bs-example-navbar-collapse-1\">
                    <ul class=\"nav navbar-nav\">
                        <li class=\"active\"><a href=\"";
        // line 25
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">Accueil</a></li>
                        <li><a href=\"";
        // line 26
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("pingouin_list");
        echo "\">
                                Liste des pingouins</a></li>
                        <li><a href=\"";
        // line 28
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_registration_register");
        echo "\"> S'inscrire</a></li>
                        <li><a href=\"";
        // line 29
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
        echo "\">Connexion</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>
        </div>
    </div>
</div>";
        
        $__internal_5ef6f5767758e1b9acc59d8543f78066c44fd2d5432a1cb9e6a3d49d3dc7df1b->leave($__internal_5ef6f5767758e1b9acc59d8543f78066c44fd2d5432a1cb9e6a3d49d3dc7df1b_prof);

        
        $__internal_c1f47c7999d49f7f47cd479411d29e0278b4faa59cb6af4ad0672a5aa627818d->leave($__internal_c1f47c7999d49f7f47cd479411d29e0278b4faa59cb6af4ad0672a5aa627818d_prof);

    }

    public function getTemplateName()
    {
        return "FrontBundle:UI:menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 29,  63 => 28,  58 => 26,  54 => 25,  41 => 15,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"header\">
    <div class=\"container\">
        <div class=\"header-nav\">
            <nav class=\"navbar navbar-default\">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\"
                            data-target=\"#bs-example-navbar-collapse-1\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <div class=\"logo\">
                        <a class=\"navbar-brand\" href=\"{{ path('homepage') }}\">Pingouin <span>Base</span></a>
                        <div class=\"leaf\">
                            <span class=\"glyphicon glyphicon-leaf\" aria-hidden=\"true\"></span>
                        </div>
                    </div>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class=\"collapse navbar-collapse nav-wil\" id=\"bs-example-navbar-collapse-1\">
                    <ul class=\"nav navbar-nav\">
                        <li class=\"active\"><a href=\"{{ path('homepage') }}\">Accueil</a></li>
                        <li><a href=\"{{ path('pingouin_list') }}\">
                                Liste des pingouins</a></li>
                        <li><a href=\"{{ path('fos_user_registration_register') }}\"> S'inscrire</a></li>
                        <li><a href=\"{{ path('fos_user_security_login') }}\">Connexion</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </nav>
        </div>
    </div>
</div>", "FrontBundle:UI:menu.html.twig", "/vagrant/pingouin/src/FrontBundle/Resources/views/UI/menu.html.twig");
    }
}
