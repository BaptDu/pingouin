<?php

/* FrontBundle:UI:banner.html.twig */
class __TwigTemplate_bedf459f44a62b838792c37bd2d6455e3b80797233b4d658c4b4a960df7deb0a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4dece5dde9af8a972b4b5945767561973466974c053f91182963c87a73c2300e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4dece5dde9af8a972b4b5945767561973466974c053f91182963c87a73c2300e->enter($__internal_4dece5dde9af8a972b4b5945767561973466974c053f91182963c87a73c2300e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontBundle:UI:banner.html.twig"));

        $__internal_03ac9fad7c91626628be8795bc0f30caeb6c586188d07022bfbe09f5d7cac76c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_03ac9fad7c91626628be8795bc0f30caeb6c586188d07022bfbe09f5d7cac76c->enter($__internal_03ac9fad7c91626628be8795bc0f30caeb6c586188d07022bfbe09f5d7cac76c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontBundle:UI:banner.html.twig"));

        // line 1
        echo "<div class=\"banner\">
    <div class=\"container\">
        <section class=\"slider\">
            <div class=\"flexslider\">
                <ul class=\"slides\">
                    <li>
                        <div class=\"banner-info\">
                            <h1>
                                Découvre les pingouins</h1>
                            <p>Faites un tour sur notre site <strong>PingouinBase</strong>, vous découvrirez des merveilles sur les pingouins et les mystères qui les entourent</p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <!--FlexSlider-->
        <div class=\"donate\">
            <a href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("pingouin_list");
        echo "\">Liste des pingouins</a>
        </div>
    </div>
</div>";
        
        $__internal_4dece5dde9af8a972b4b5945767561973466974c053f91182963c87a73c2300e->leave($__internal_4dece5dde9af8a972b4b5945767561973466974c053f91182963c87a73c2300e_prof);

        
        $__internal_03ac9fad7c91626628be8795bc0f30caeb6c586188d07022bfbe09f5d7cac76c->leave($__internal_03ac9fad7c91626628be8795bc0f30caeb6c586188d07022bfbe09f5d7cac76c_prof);

    }

    public function getTemplateName()
    {
        return "FrontBundle:UI:banner.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 18,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"banner\">
    <div class=\"container\">
        <section class=\"slider\">
            <div class=\"flexslider\">
                <ul class=\"slides\">
                    <li>
                        <div class=\"banner-info\">
                            <h1>
                                Découvre les pingouins</h1>
                            <p>Faites un tour sur notre site <strong>PingouinBase</strong>, vous découvrirez des merveilles sur les pingouins et les mystères qui les entourent</p>
                        </div>
                    </li>
                </ul>
            </div>
        </section>
        <!--FlexSlider-->
        <div class=\"donate\">
            <a href=\"{{ path('pingouin_list') }}\">Liste des pingouins</a>
        </div>
    </div>
</div>", "FrontBundle:UI:banner.html.twig", "/vagrant/pingouin/src/FrontBundle/Resources/views/UI/banner.html.twig");
    }
}
