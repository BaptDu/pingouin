<?php

/* FrontBundle::base.html.twig */
class __TwigTemplate_4626cc3a883fa92486acdca48fe7e0c8e7ef7232c7d10fe871f6cc4cae04b948 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'childtitle' => array($this, 'block_childtitle'),
            'content' => array($this, 'block_content'),
            'javascript' => array($this, 'block_javascript'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9eec64520588fa071e94fc97578a22b018442d62172bb76b9bd0080ad79b27d0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9eec64520588fa071e94fc97578a22b018442d62172bb76b9bd0080ad79b27d0->enter($__internal_9eec64520588fa071e94fc97578a22b018442d62172bb76b9bd0080ad79b27d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontBundle::base.html.twig"));

        $__internal_d34d2e657fab8d6e8b89238ac27b40675f186a02866070232649301f80cb7c7e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d34d2e657fab8d6e8b89238ac27b40675f186a02866070232649301f80cb7c7e->enter($__internal_d34d2e657fab8d6e8b89238ac27b40675f186a02866070232649301f80cb7c7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontBundle::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"en\">
<head>
    <title>";
        // line 4
        $this->displayBlock('childtitle', $context, $blocks);
        echo " - Des Pingouins</title>
    <!-- Bootstrap core CSS -->
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">

    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>
    <meta name=\"keywords\" content=\"Fashion Club Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design\"/>
    <script type=\"application/x-javascript\"> addEventListener(\"load\", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!-- css -->
    <link href=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/style.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\"/>
    <link rel=\"stylesheet\" href=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/font-awesome.min.css"), "html", null, true);
        echo "\" type=\"text/css\" media=\"all\"/>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/css/jquery.lightbox.css"), "html", null, true);
        echo "\">
    <!--// css -->
    <!-- font -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
          rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Abril+Fatface' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Quicksand:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Custom styles for this template -->
</head>
<body>
";
        // line 33
        $this->loadTemplate("FrontBundle:UI:menu.html.twig", "FrontBundle::base.html.twig", 33)->display($context);
        // line 34
        $this->loadTemplate("FrontBundle:UI:banner.html.twig", "FrontBundle::base.html.twig", 34)->display($context);
        // line 35
        $this->displayBlock('content', $context, $blocks);
        // line 36
        $this->loadTemplate("FrontBundle:UI:footer.html.twig", "FrontBundle::base.html.twig", 36)->display($context);
        // line 37
        echo "

</body>
";
        // line 40
        $this->displayBlock('javascript', $context, $blocks);
        // line 56
        echo "</html>";
        
        $__internal_9eec64520588fa071e94fc97578a22b018442d62172bb76b9bd0080ad79b27d0->leave($__internal_9eec64520588fa071e94fc97578a22b018442d62172bb76b9bd0080ad79b27d0_prof);

        
        $__internal_d34d2e657fab8d6e8b89238ac27b40675f186a02866070232649301f80cb7c7e->leave($__internal_d34d2e657fab8d6e8b89238ac27b40675f186a02866070232649301f80cb7c7e_prof);

    }

    // line 4
    public function block_childtitle($context, array $blocks = array())
    {
        $__internal_1150acc12990c5a27f4445d5556f94f976c45bfbf5400f2db582b29e666104e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1150acc12990c5a27f4445d5556f94f976c45bfbf5400f2db582b29e666104e5->enter($__internal_1150acc12990c5a27f4445d5556f94f976c45bfbf5400f2db582b29e666104e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "childtitle"));

        $__internal_bcd003ee18d020773128aef935d3b46e332eb656180115ef487de96230861f9b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bcd003ee18d020773128aef935d3b46e332eb656180115ef487de96230861f9b->enter($__internal_bcd003ee18d020773128aef935d3b46e332eb656180115ef487de96230861f9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "childtitle"));

        
        $__internal_bcd003ee18d020773128aef935d3b46e332eb656180115ef487de96230861f9b->leave($__internal_bcd003ee18d020773128aef935d3b46e332eb656180115ef487de96230861f9b_prof);

        
        $__internal_1150acc12990c5a27f4445d5556f94f976c45bfbf5400f2db582b29e666104e5->leave($__internal_1150acc12990c5a27f4445d5556f94f976c45bfbf5400f2db582b29e666104e5_prof);

    }

    // line 35
    public function block_content($context, array $blocks = array())
    {
        $__internal_2323a2de98253e2a2dec40f61e2748d596f94c9975c0d7f4655c170d62550040 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2323a2de98253e2a2dec40f61e2748d596f94c9975c0d7f4655c170d62550040->enter($__internal_2323a2de98253e2a2dec40f61e2748d596f94c9975c0d7f4655c170d62550040_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_d705cd5b4c9674bf53419fe345ee2ef1e60c08b28b6a33100efdf5b1d47af39f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d705cd5b4c9674bf53419fe345ee2ef1e60c08b28b6a33100efdf5b1d47af39f->enter($__internal_d705cd5b4c9674bf53419fe345ee2ef1e60c08b28b6a33100efdf5b1d47af39f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        
        $__internal_d705cd5b4c9674bf53419fe345ee2ef1e60c08b28b6a33100efdf5b1d47af39f->leave($__internal_d705cd5b4c9674bf53419fe345ee2ef1e60c08b28b6a33100efdf5b1d47af39f_prof);

        
        $__internal_2323a2de98253e2a2dec40f61e2748d596f94c9975c0d7f4655c170d62550040->leave($__internal_2323a2de98253e2a2dec40f61e2748d596f94c9975c0d7f4655c170d62550040_prof);

    }

    // line 40
    public function block_javascript($context, array $blocks = array())
    {
        $__internal_650252cf0e8d34b2642ce929db1ae1042fa02fe0811ef7d7e79a73bc1324d86a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_650252cf0e8d34b2642ce929db1ae1042fa02fe0811ef7d7e79a73bc1324d86a->enter($__internal_650252cf0e8d34b2642ce929db1ae1042fa02fe0811ef7d7e79a73bc1324d86a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        $__internal_56208ef24090b0457bc369bd942a7ec6463c15892e7466d3bbb44b6868966f47 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56208ef24090b0457bc369bd942a7ec6463c15892e7466d3bbb44b6868966f47->enter($__internal_56208ef24090b0457bc369bd942a7ec6463c15892e7466d3bbb44b6868966f47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascript"));

        // line 41
        echo "    <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\"
            integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js\"
            integrity=\"sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js\"
            integrity=\"sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1\"
            crossorigin=\"anonymous\"></script>
    <script src=\"";
        // line 50
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/navbar.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 51
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery-1.11.1.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 52
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <!-- pop-up-box -->
    <script src=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("assets/js/jquery.lightbox.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_56208ef24090b0457bc369bd942a7ec6463c15892e7466d3bbb44b6868966f47->leave($__internal_56208ef24090b0457bc369bd942a7ec6463c15892e7466d3bbb44b6868966f47_prof);

        
        $__internal_650252cf0e8d34b2642ce929db1ae1042fa02fe0811ef7d7e79a73bc1324d86a->leave($__internal_650252cf0e8d34b2642ce929db1ae1042fa02fe0811ef7d7e79a73bc1324d86a_prof);

    }

    public function getTemplateName()
    {
        return "FrontBundle::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  169 => 54,  164 => 52,  160 => 51,  156 => 50,  145 => 41,  136 => 40,  119 => 35,  102 => 4,  92 => 56,  90 => 40,  85 => 37,  83 => 36,  81 => 35,  79 => 34,  77 => 33,  64 => 23,  60 => 22,  56 => 21,  52 => 20,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"en\">
<head>
    <title>{% block childtitle %}{% endblock %} - Des Pingouins</title>
    <!-- Bootstrap core CSS -->
    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">

    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>
    <meta name=\"keywords\" content=\"Fashion Club Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design\"/>
    <script type=\"application/x-javascript\"> addEventListener(\"load\", function () {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        } </script>
    <!-- css -->
    <link href=\"{{ asset('assets/css/bootstrap.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('assets/css/style.css') }}\" type=\"text/css\" media=\"all\"/>
    <link rel=\"stylesheet\" href=\"{{ asset('assets/css/font-awesome.min.css') }}\" type=\"text/css\" media=\"all\"/>
    <link rel=\"stylesheet\" type=\"text/css\" href=\"{{ asset('assets/css/jquery.lightbox.css') }}\">
    <!--// css -->
    <!-- font -->
    <link href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
          rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Abril+Fatface' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Quicksand:300,400,700' rel='stylesheet' type='text/css'>
    <!-- Custom styles for this template -->
</head>
<body>
{% include \"FrontBundle:UI:menu.html.twig\" %}
{% include \"FrontBundle:UI:banner.html.twig\" %}
{% block content %}{% endblock %}
{% include \"FrontBundle:UI:footer.html.twig\" %}


</body>
{% block javascript %}
    <script src=\"https://code.jquery.com/jquery-3.2.1.slim.min.js\"
            integrity=\"sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js\"
            integrity=\"sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4\"
            crossorigin=\"anonymous\"></script>
    <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js\"
            integrity=\"sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1\"
            crossorigin=\"anonymous\"></script>
    <script src=\"{{ asset('assets/js/navbar.js') }}\"></script>
    <script src=\"{{ asset('assets/js/jquery-1.11.1.min.js') }}\"></script>
    <script src=\"{{ asset('assets/js/bootstrap.js') }}\"></script>
    <!-- pop-up-box -->
    <script src=\"{{ asset('assets/js/jquery.lightbox.js') }}\"></script>
{% endblock %}
</html>", "FrontBundle::base.html.twig", "/vagrant/pingouin/src/FrontBundle/Resources/views/base.html.twig");
    }
}
