<?php

/* FrontBundle:UI:footer.html.twig */
class __TwigTemplate_53727a8d6917d7a666d4641f3ea23dc36a56ac7650159a886bf0f102ee0f2c74 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8d500faa9afe1837747f79659968ce65db712b991538f1c759740c662e882579 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8d500faa9afe1837747f79659968ce65db712b991538f1c759740c662e882579->enter($__internal_8d500faa9afe1837747f79659968ce65db712b991538f1c759740c662e882579_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontBundle:UI:footer.html.twig"));

        $__internal_5e7499fd86e7b1cd291689cded265050787c0b68592113b34010ad1454608a23 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5e7499fd86e7b1cd291689cded265050787c0b68592113b34010ad1454608a23->enter($__internal_5e7499fd86e7b1cd291689cded265050787c0b68592113b34010ad1454608a23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FrontBundle:UI:footer.html.twig"));

        // line 1
        echo "<footer>
        <div class=\"copy\">
            <p>Copyright © 2017. PingouinBase All rights reserved
        </div>
</footer>";
        
        $__internal_8d500faa9afe1837747f79659968ce65db712b991538f1c759740c662e882579->leave($__internal_8d500faa9afe1837747f79659968ce65db712b991538f1c759740c662e882579_prof);

        
        $__internal_5e7499fd86e7b1cd291689cded265050787c0b68592113b34010ad1454608a23->leave($__internal_5e7499fd86e7b1cd291689cded265050787c0b68592113b34010ad1454608a23_prof);

    }

    public function getTemplateName()
    {
        return "FrontBundle:UI:footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<footer>
        <div class=\"copy\">
            <p>Copyright © 2017. PingouinBase All rights reserved
        </div>
</footer>", "FrontBundle:UI:footer.html.twig", "/vagrant/pingouin/src/FrontBundle/Resources/views/UI/footer.html.twig");
    }
}
