<?php

namespace FrontBundle\Controller;

use ReviewBundle\Entity\Review;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use PingouinBundle\Repository\PingouinRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use PingouinBundle\Entity\Pingouin;

class DefaultController extends Controller
{
    /**
     * @Route("/" , name="homepage")
     */
    public function indexAction()
    {
        return $this->render('FrontBundle:Default:index.html.twig');

    }

    /**
     * @Route("/detail/{id}", name="pingouin_detail")
     */
    public function detailAction($id, Request $request)
    {
        $pingouinRepository = $this->getDoctrine()->getRepository(Pingouin::class);
        $pingouin = $pingouinRepository->find($id);

        $reviewRepository = $this->getDoctrine()->getRepository(Review::class);
        $review= $reviewRepository->find($id);

        $user = $this->getUser();
        if ($user) {
            $reviewForm = $this->getReviewForm();

            $reviewForm->handleRequest($request);
            if ($reviewForm->isSubmitted() && $reviewForm->isValid()) {
                $review = $reviewForm->getData();

                $review->setUser($this->getUser());
                $review->setpingouin($pingouin);
                $em = $this->getDoctrine()->getManager();
                $em->persist($review);
                $em->flush();
                return $this->redirectToRoute('pingouin_detail', [
                    'id' => $id,
                ]);
            }
        }




        return $this->render('FrontBundle:Detail:detail.html.twig', [
            'pingouin' => $pingouin,
            'review' => $review,
            'form_review' => $user ? $reviewForm->createView() : null,
        ]);
    }

    private function getReviewForm()
    {
        $review = new Review();
        $form = $this->createFormBuilder($review)
            ->add('rating', IntegerType::class,
                ['label' => "Note sur 10"],
                ['invalid_message' => 'You entered an invalid value, it should include %num% letters']
            )
            ->add('comment', TextareaType::class, [
                'label' => "Commentaire",
                'required' => false,
            ])
            ->add('save', SubmitType::class, array(
                'label' => "Noter le pingouin")
            )
            ->getForm()
        ;

        return $form;
    }

    /**
     * @Route("/list", name="pingouin_list")
     */
    public function listAction()
    {
        $pingouinRepository = $this->getDoctrine()->getRepository(Pingouin::class);
        $pingouins = $pingouinRepository->findAll();

        return $this->render('FrontBundle:List:list.html.twig', [
            'pingouins' => $pingouins,
        ]);
    }


    public function CommentListAction()
    {
        $reviewRepository = $this->getDoctrine()->getRepository(Review::class);
        $reviews = $reviewRepository->findAll();

        return $this->render('FrontBundle:Detail:detail.html.twig', [
            'reviews' => $reviews,
        ]);


    }
}
