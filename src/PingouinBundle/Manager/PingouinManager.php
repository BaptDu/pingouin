<?php

namespace PingouinBundle\Manager;

use PingouinBundle\Entity\Pingouin;
use PingouinBundle\Form\PingouinType;
use PingouinBundle\Form\ReviewType;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class PingouinManager
 * @package PingouinBundle\Manager
 */
class PingouinManager extends BaseManager
{

    /**
     * PingouinManager constructor.
     * @param $em
     * @param $formFactory
     * @param Router $router
     */
    public function __construct($em, $formFactory, Router $router)
    {
        parent::__construct($em, $formFactory, $router);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->em->getRepository('PingouinBundle:Pingouin')->findOneBy(array('id' => $id));
    }

    /**
     * @return mixed
     */
    public function findAll()
    {
        return $this->em->getRepository('PingouinBundle:Pingouin')->findAll();
    }

    /**
     * @param Request $request
     * @return array
     */
    public function save(Request $request)
    {
        $pingouin = new Pingouin();
        return $this->handleForm($request, $pingouin);
    }

    /**
     * @param Request $request
     * @param $id
     * @return array|RedirectResponse
     */
    public function edit(Request $request, $id)
    {
        $pingouin = $this->em->getRepository('PingouinBundle:Pingouin')->find($id);
        return $this->handleForm($request, $pingouin);
    }

    /**
     * @param $id
     * @return RedirectResponse
     * @internal param Request $request
     */
    public function delete($request, $id)
    {
        $pingouin = $this->em->getRepository('PingouinBundle:Pingouin')->find($id);

        if($pingouin instanceof Pingouin)
        {
            $this->removeAndFlush($pingouin);
            return $this->redirect('pingouin_index');
        }else
        {
            throw new HttpException(418);
        }

    }

    /**
     * @param Request $request
     * @param Pingouin $pingouin
     * @return array|RedirectResponse
     */
    public function handleForm(Request $request, $pingouin)
    {
        $form = $this->formFactory->create(PingouinType::class, $pingouin);
        return $this->handleBaseForm($request, $form, $pingouin, "pingouin_index");
    }

}