<?php

namespace PingouinBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ReviewBundle\Entity\Review;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpFoundation\Request;
use PingouinBundle\Entity\Pingouin;
use Symfony\Component\HttpFoundation\Response;
use PingouinBundle\Repository\PingouinRepository;


/**
 * @Route("/pingouin")
 */
class PingouinController extends Controller
{

    /**
     * @Template()
     * @Route("/", name="pingouin_index")
     * @return array
     */
    public function indexAction()
    {
        $pingouins = $this->get('app.pingouins.manager')->findAll();
        return array('pingouins' => $pingouins);
    }

    /**
     * @Template()
     * @Route("/new", name="pingouin_create")
     * @param Request $request
     * @return array
     */
    public function newAction(Request $request)
    {
        return $this->get('app.pingouins.manager')->save($request);
    }

    /**
     * @Template()
     * @Route("/{id}/edit", name="pingouin_edit")
     * @param Request $request
     * @param $id
     * @return array
     */
    public function editAction(Request $request, $id)
    {
        return $this->get('app.pingouins.manager')->edit($request, $id);

    }

    /**
     * @Template()
     * @Route("/{id}/delete", name="pingouin_delete")
     * @param Request $request
     * @param $id
     * @return array
     */
    public function deleteAction(Request $request, $id)
    {
        return $this->get('app.pingouins.manager')->delete($request, $id);
    }

}
