<?php

namespace PingouinBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pingouin
 *
 * @ORM\Table(name="pingouin")
 * @ORM\Entity(repositoryClass="PingouinBundle\Repository\PingouinRepository")
 */
class Pingouin
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     *
     * @ORM\OneToMany(targetEntity="ReviewBundle\Entity\Review", mappedBy="pingouin")
     */
    private $reviews;

    /**
     * @var string
     *
     * @ORM\Column(name="specie", type="string", length=255)
     */
    private $specie;

    /**
     * @var string
     *
     * @ORM\Column(name="summary", type="text")
     */
    private $summary;

    /**
     * @var string
     *
     * @ORM\Column(name="picture", type="string", length=255)
     */
    private $picture;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Pingouin
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Set reviews
     *
     * @param array $reviews
     *
     * @return Pingouin
     */
    public function setReviews($reviews)
    {
        $this->reviews = $reviews;
        return $this;
    }

    /**
     * Get reviews
     *
     * @return array
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set specie
     *
     * @param string $specie
     *
     * @return Pingouin
     */
    public function setSpecie($specie)
    {
        $this->specie = $specie;

        return $this;
    }

    /**
     * Get specie
     *
     * @return string
     */
    public function getSpecie()
    {
        return $this->specie;
    }

    /**
     * Set summary
     *
     * @param string $summary
     *
     * @return Pingouin
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * Get summary
     *
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * Set picture
     *
     * @param string $picture
     *
     * @return Pingouin
     */
    public function setPicture($picture = null)
    {
        $this->picture = $picture;

        return $this;
    }

    /**
     * Get picture
     *
     * @return string
     */
    public function getPicture()
    {
        $name = strtoupper($this->name);
        $name = str_replace('é', 'E',  $name);

        return sprintf(
            'bundles/pingouin/img/%s.jpg',
            $name
        );

    }

    public function getAverageRating(): int
    {
        $ratings = [];
        foreach ($this->getReviews() as $review) {
            $ratings[] = $review->getRating();
        }

        if (count($ratings) === 0) {
            return 0;
        }

        return round(array_sum($ratings) / count($ratings));
    }

}

