<?php

namespace FixtureBundle\Command;

use PingouinBundle\Entity\Pingouin;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use UserBundle\Entity\User;
use ReviewBundle\Entity\Review;

class FixturesCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:fixtures')
            ->setDescription('Chargement des données')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->importData($output);
    }

    private function importData($output)
    {

        $entityManager = $this->getContainer()->get('doctrine.orm.entity_manager');

        $loader = new \Nelmio\Alice\Loader\NativeLoader();
        $objectSet = $loader->loadFile('/vagrant/test/pingouin/src/ReviewBundle/Resources/fixtures/rating.yml');
        foreach ($objectSet->getObjects() as $entity){
            $entityManager->persist($entity);
        }

        $entityManager->flush();
        $output->writeln('<info>Import reviews OK !</info>');
        $output->writeln('<info>Import User OK !</info>');
        $output->writeln('<info>Import Pingouin OK !</info>');
    }

}
